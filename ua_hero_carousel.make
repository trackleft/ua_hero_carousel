; ------------------------------------------------------------------------------
; UA Hero Carousel Makefile
;
; Downloads contrib module and library dependencies for UA Featured Content
; component.
; ------------------------------------------------------------------------------

core = 7.x
api = 2

; Set default contrib destination
defaults[projects][subdir] = contrib

; ------------------------------------------------------------------------------
; Contrib modules
; ------------------------------------------------------------------------------

projects[flexslider][version] = 2.0-alpha3

; ------------------------------------------------------------------------------
; Libraries
; ------------------------------------------------------------------------------

libraries[flexslider][download][type] = get
libraries[flexslider][download][url] = https://github.com/woothemes/FlexSlider/archive/version/2.4.0.zip
libraries[flexslider][directory_name] = flexslider
